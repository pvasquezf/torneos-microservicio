from django.contrib import admin
from .models import Torneo, Partida

# Register your models here.
admin.site.register(Torneo)
admin.site.register(Partida)